def clasificar_gramaticas(gramatica):
    lista = []
    resultado = {
        '3': [],
        '2': [],
        '1': [],
        '0': [],
    }
    ErroresG3 = ["No pertenece a la regla = Nt -> t", "No pertenece a la regla = Nt ->Nt t",
                 "No pertenece a la regla = Nt ->t Nt"]
    errorG2 = "No pertenece a la regla = NT -> v, siendo v cualquier combinacion de NT y t, incluyendo vacio(lambda)"
    errorG1 = ["No cumple con el criterio de los autores Alfonseca y otros.",
               "La combinacion de NT y t del Antecedente es mayor a la del Concecuente",
               "Solo el distinguido se podria derivar por lambda"]
    Rrecursiva = []
    Rlambda = []
    lista = gramatica.split("\n")
    primerRegla = lista[0].split(":")
    S = primerRegla[0].split(" ")
    verificarRecursion = False

    for regla in lista:
        div = regla.split(":")
        ante = div[0].split(" ")
        cons = div[1].split(" ")

        if len(ante) > len(cons):
            resultado['1'].append((regla, errorG1[1]))

        for c in cons:
            if c == S[0]:
                Rrecursiva.append(regla)

        if len(ante) > 1:
            resultado['3'].append((regla, ErroresG3[0]))
            resultado['2'].append((regla, errorG2))
        else:
            if ante[0] == ante[0].lower():
                resultado['3'].append((regla, ErroresG3[0]))
                resultado['2'].append((regla, errorG2))

        if len(cons) > 2:
            if cons[0] == cons[0].lower():
                resultado['3'].append((regla, ErroresG3[2]))
            else:
                resultado['3'].append((regla, ErroresG3[1]))
        else:
            if cons[0] != "lambda":
                NT = 0
                t = 0
                for c in cons:
                    if c == c.lower():
                        t += 1
                    if c == c.upper():
                        NT += 1
                if t > 1:
                    resultado['3'].append((regla, ErroresG3[2]))
                if NT > 1:
                    resultado['3'].append((regla, ErroresG3[1]))
            else:
                if ante[0] != S[0]:
                    Rlambda.append(regla)
                else:
                    verificarRecursion = True

    for r in Rlambda:
        resultado['1'].append((r, errorG1[2]))
    if verificarRecursion:
        for r in Rrecursiva:
            resultado['1'].append((r, errorG1[0]))

    return resultado

class AutomataPila:
    """ Esta clase implementa un automáta de pila a partir de la definición de
    estados y transiciones que lo componen, pudiendo validar si una cadena dada
    puede ser reconocida por el mismo.
    """

    def __init__(self, estados, estados_aceptacion):
        """ Constructor de la clase.

        Args
        ----
        estados: dict
            Diccionario de estados que especifica en las claves los nombres de los
            estados y como valores una lista de transiciones salientes de dicho estado.
            Cada transición se compone de: (s,p,a,e) siendo
            s -> símbolo que se consume de la entrada para aplicar la transición.
            p -> símbolo que se consume del tope de la pila para aplicar la transición.
            a -> lista de símbolo/s que se apila una vez aplicada la transición.
            e -> estado de destino.

            Ejemplo:
            {'a': [('(', 'Z0', ['Z0'], 'a'),
                   ('(', '(', ['(', '('], 'a'),
                   (')', '(', [''], 'b')],
             'b': [(')', '(', [''], 'b'),
                   ('$', 'Z0', ['Z0'], 'b')]}

        estados_aceptacion: array-like
            Estados que admiten fin de cadena.

            Ejemplo:
            ['b']
        """
        self.estadosAceptacion = estados_aceptacion
        self.estados = estados
        self.estado_actual = None
        self.cadena_restante = ''
        self.pila = ["Z0"]

    def validar_cadena(self, cadena):
        self.cadena_restante = cadena
        Valida = True
        while Valida and self.cadena_restante != "":
            L = self.cadena_restante[0]
            self.cadena_restante = self.cadena_restante[1:]
            Valida = self.TransicionValida(L)

        if not Valida:
            return Valida

        existe = False
        for ea in self.estadosAceptacion:
            if ea == self.estado_actual:
                existe = True
        Valida = existe
        return Valida

    def TransicionValida(self, l):
        tope = self.pila.pop()
        if self.estado_actual == None:
            for e in self.estados:
                for t in self.estados[e]:
                    if (t[1] == tope or t[1] == '') and t[0] == l:
                        if t[1] == '':
                            self.pila.append(tope)
                        self.estado_actual = t[3]
                        for apila in t[2]:
                            if apila != '':
                                self.pila.append(apila)
                        return True
        else:
            for t in self.estados[self.estado_actual]:
                if (t[1] == tope or t[1] == '') and t[0] == l:
                    if t[1] == '':
                        self.pila.append(tope)
                    self.estado_actual = t[3]
                    for apila in t[2]:
                        if apila != '':
                            self.pila.append(apila)
                    return True
        return False


"""
    PRUEBAS
    
    estados = {'A': [('a', '', ['a'], 'A'),
                 ('b', '', ['b'], 'A'),
                 ('c', 'a', ['a'], 'B'),
                 ('c', 'b', ['b'], 'B')],
           'B': [('b', 'b', [''], 'B'),
                 ('a', 'a', [''], 'B'),
                 ('$', 'Z0', ['Z0'], 'B')]}
estadosAcep = ["B"]
Automata = AutomataPila(estados, estadosAcep)

resultado = Automata.validar_cadena("abbcbba$")

algo = clasificar_gramaticas("A:b\nA:A a\nA B:B c\nA:lambda\nB:b")
"""



